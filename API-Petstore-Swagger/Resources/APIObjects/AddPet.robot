*** Settings ***
Resource   Core/CommonResources/Utilities.robot

*** Variables ***

*** Keywords ***
AddPet.create the test data to run the API
    [Arguments]      ${AddPet_Name}    ${AddPet_AStatus}    ${AddPet_ID}=3
    ${json_TestData}=	Get File  	${CURDIR}/AddPet.json
    ${json_TestData} =	Replace String	 ${json_TestData}	<<AnimalID>> 	${AddPet_ID}
    ${json_TestData} =	Replace String	 ${json_TestData}	<<AnimalName>> 	${AddPet_Name}
    ${json_TestData} =	Replace String	 ${json_TestData}	<<AnimalStatus>> 	${AddPet_AStatus}
    Set test variable    ${json_TestData}
    &{header}=  Create Dictionary  Content-Type=application/json  Accept-Encoding=gzip, deflate, br
    Set test variable    ${header}


AddPet.run the Post Service
    [Arguments]      ${endPoint}

     ${post_Response}=  Post On Session  ${MySession}   ${endPoint}   data=${json_TestData}   headers=${header}
     Set test Variable   ${post_Response}
