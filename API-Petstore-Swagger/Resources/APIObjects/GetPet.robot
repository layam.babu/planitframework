*** Settings ***
Resource   Core/CommonResources/Utilities.robot

*** Variables ***

*** Keywords ***
GetPet.Prepare the API URL
    [Arguments]      ${endPoint}
#    Log to console   The actual end point for the Get Pet API is ${PetStoreSwagger_API_URL} ${endPoint}
    Log    The actual end point for the Get Pet API is ${PetStoreSwagger_API_URL} ${endPoint}

GetPet.Run the Get Service
    [Arguments]      ${endPoint}

    ${get_Response}=  GET On Session  ${MySession}   ${endPoint}
    Set test Variable   ${get_Response}


GetPet.Validate the Actual Responde code
    [Arguments]    ${ExpectedResponseCode}
    Status Should Be   ${ExpectedResponseCode}    ${get_Response}

GetPet.capture the response data for "Name"
    ${response_Name}=  Get value from JSON  ${get_Response.json()}   name
#    log   ${response_Name}
    ${Actual_ResponseAnimalName}=  Get From List   ${response_Name}   0
#    log   ${Actual_ResponseAnimalName}
    Set test variable   ${Actual_ResponseAnimalName}

GetPet.capture the response data for "status"
    ${response_Status}=  Get value from JSON  ${get_Response.json()}   status
#    log   ${response_Name}
    ${Actual_ResponseAnimalStatus}=  Get From List   ${response_Status}   0
#    log   ${Actual_ResponseAnimalName}
    Set test variable   ${Actual_ResponseAnimalStatus}

GetPet.validate the expected response data
    [Arguments]       ${ExpectedPet_AnimalName}
    Should be equal        ${Actual_ResponseAnimalName}     ${ExpectedPet_AnimalName}

GetPet.validate the expected response data for status
    [Arguments]       ${ExpectedPet_Status}
    Should be equal        ${Actual_ResponseAnimalStatus}     ${ExpectedPet_Status}
