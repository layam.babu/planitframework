*** Settings ***
Resource   API-Petstore-Swagger/Resources/APIObjects/GetPet.robot
Resource   API-Petstore-Swagger/Resources/APIObjects/AddPet.robot
Resource   Core/CommonResources/Utilities.robot


*** Variables ***


*** Keywords ***
I run the GetService name called - "Get Pet"
#    GetPet.Prepare the API URL     ${GetPet_ENDPoint}
    GetPet.Run the Get Service     ${GetPet_ENDPoint}

I validate the "Get Pet" Response code
    GetPet.Validate the Actual Responde code    ${GetPet_Expected_ResponseCode}

I validate the "Get Pet" Response body
    GetPet.capture the response data for "Name"
    GetPet.validate the expected response data     ${ExpectedPet_AnimalName}

I should have the same pet name and status in the response
    [Arguments]     ${ExpectedPet_AnimalName}   ${ExpectedPet_Status}
    GetPet.capture the response data for "Name"
    GetPet.validate the expected response data     ${ExpectedPet_AnimalName}
    GetPet.capture the response data for "status"
    GetPet.validate the expected response data for status     ${ExpectedPet_Status}


I have created a pet with the name and status as:
    [Arguments]    ${AddPet_Name}   ${AddPet_AStatus}    ${AddPet_ID}=3
    AddPet.create the test data to run the API    ${AddPet_Name}   ${AddPet_AStatus}
    AddPet.run the Post Service      ${AddPet_ENDPoint}

I get the same pet details
    I run the GetService name called - "Get Pet"