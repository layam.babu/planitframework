*** Settings ***
Documentation     API Testing in Robot Framework
Resource          Core/CommonResources/BaseTest.robot
Resource          API-Petstore-Swagger/Resources/StepDefinitions/BDDKeywords.robot
Test Setup        Base API Test Setup        mysession
#Test Teardown     Base API Test Tear Down

*** Variables ***

*** Test Cases ***
Do a POST and GET Request and validate the response code and response body
    [documentation]  This test case creates a Pet using POST Request and
    ...  verifies that the response code of the GET Request should be 200 and
    ...  the response body contains the 'name' key with value as 'AnimalName'
    [Tags]    SmokeTesting

    Given I have created a pet with the name and status as:  Cat  Available
    When I get the same pet details
    Then I should have the same pet name and status in the response   Dog   Available












