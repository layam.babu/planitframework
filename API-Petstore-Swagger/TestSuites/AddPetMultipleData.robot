*** Settings ***
Documentation     API Testing in Robot Framework
Resource          Core/CommonResources/BaseTest.robot
Resource          API-Petstore-Swagger/Resources/StepDefinitions/BDDKeywords.robot
Test Setup        Base API Test Setup        mysession
Force Tags        RegressionTesting
#Test Teardown     Base API Test Tear Down
Test Template      Do a POST and GET Request and validate the response code and response body

| *** Test Cases *** |  |           |   |
| TC1      | Dog        | Available | 1 |
| TC2      | Cat        | Available | 2 |
| TC3      | Elk        | Available | 3 |
| TC4      | Ant        | Available | 4 |
| TC5      | Cow        | Available | 5 |
| TC6      | Pig        | Available | 6 |
| TC7      | Bee        | Available | 7 |
| TC8      | EEL        | Available | 8 |
| TC9      | OWL        | Available | 9 |


*** Keywords ***
Do a POST and GET Request and validate the response code and response body
    [documentation]  This test case creates a Pet using POST Request and
    ...  verifies that the response code of the GET Request should be 200 and
    ...  the response body contains the 'name' key with value as 'AnimalName'
    [Arguments]    ${name}  ${Status}   ${ID}

    Given I have created a pet with the name and status as:  ${name}  ${Status}   ${ID}
    When I get the same pet details
    Then I should have the same pet name and status in the response   ${name}  ${Status}











    log   The actual and expected animal is ${name}
    log to console   The actual and expected animal is ${name}









