*** Settings ***
Library           SeleniumLibrary
Library           OperatingSystem
Library           RequestsLibrary
Library           JSONLibrary
Library           Collections
Library           String
Library           Pdf2TextLibrary

*** Variables ***

*** Keywords ***
Base Web Test Setup
    [Arguments]    ${ENV_BROWSERTYPE}

    open browser        browser=${ENV_BROWSERTYPE}

    ${originalImpilcitWait}     set selenium implicit wait   5 seconds
    set test variable    ${originalImpilcitWait}

    maximize browser window

    delete all cookies


Base Web Test Tear Down
    close all browsers
#    Utilities.Delete all Temperory Files

Base API Test Setup
    [Arguments]    ${MySession}
    Create Session  ${MySession}   ${PetStoreSwagger_API_URL}  verify=true
    Set Test Variable    ${MySession}

Base API Test Tear Down
#    To Do - Session Discconect to complete