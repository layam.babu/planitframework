*** Settings ***
Library             SeleniumLibrary
Library             String
Library             ExcelLibrary

*** Variables ***

*** Keywords ***
Utilities.Generate random applicant name
    [Documentation]
    ...   This is a generic method and can be used across enterprise
    ...   example use as below to generate 5 character random string appeneded with Auto
    [Arguments]    ${strDigits}

    ${rndName}=  Generate random string   ${strDigits}   [LETTERS]
    ${rndName}=  convert to lowercase    ${rndName}
    ${rndName}=  catenate   Auto  ${rndName}
    #${rndName}=  Replace String    ${rndName}    ${SPACE}   ${EMPTY}
    [Return]   ${rndName}

Utilities.log the script activities into Text File
    [Arguments]  ${text}
    ${CurrentTime}=   get Time
    append to file     ${LogFileName}    ${\n}${CurrentTime} - ${text}


Utilities.convert PDF to text
    [Arguments]       ${FilePath_PDF}

    ${PDF_Data_detail}     Convert Pdf To Txt              ${FilePath_PDF}
    Set Test Variable     ${PDF_Data_detail}      ${PDF_Data_detail}

Utilities.Open excel file and read row and column count
    [Arguments]    ${fileName}   ${sheetName}
    #provide path of Excel file to Open Excel
    Open Excel	  ${fileName}
    # Reading No of Columns in Excel Sheet
    ${strColCount} =  Get Column Count    ${sheetName}
    Set Test Variable   ${Col_ID}   ${strColCount}
    ${strRowCount}   Get Row Count       ${sheetName}
    Set Test Variable   ${Row_ID}   ${strRowCount}







#Utilities.Delete all Temperory Files
#    ${PATH}   get temp folder path
#    log    ${PATH}
#
#    @{items}     OperatingSystem.list directories in directory    ${PATH}
#    FOR   {ELEMENT}   IN    @{ITEMS}
#        run keyword and ignore error    Remove Directory      ${PATH}/{ELEMENT}    recursive=True
#    END
#
#    @{items}     OperatingSystem.list files in directory    ${PATH}
#    FOR   {ELEMENT}   IN    @{ITEMS}
#        run keyword and ignore error    Remove files      ${PATH}/{ELEMENT}
#    END