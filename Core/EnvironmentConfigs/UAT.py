# ******** MLC **********
CONSTANT_Customer_Number = "Customer number"







# ENV_BROWSERTYPE = "Firefox"
ENV_BROWSERTYPE = "chrome"

# ---------------API-Petstore-Swagger
PetStoreSwagger_API_URL = "https://petstore.swagger.io"
GetPet_ENDPoint = "/v2/pet/3"
GetPet_Expected_ResponseCode = "200"
ExpectedPet_AnimalName = "Cow"
ExpectedPet_Status = "Available"

AddPet_ENDPoint = "/v2/pet"
AddPet_AnimalName = "Dog"
AddPet_Status = "Available"




LogFileName = "log.txt"

# ***********************Portfolio/Application - SG Fleet**************************************
SG_LOGIN_URL = "https://www.sgfleet.com/au"
MENULINK_AboutUs = "About us"
MENULINK_AboutUs_OurStory = "Our story"
MENULINK_AboutUs_GlobalFleetManagement = "Global fleet management"
MENULINK_AboutUs_WhyChooseUs = "Why choose us"
MENULINK_AboutUs_CorporateSocialResponsibility = "Corporate social responsibility"
MENULINK_AboutUs_News = "News"
# Next Menu
MENULINK_FleetSolutions = "Fleet solutions"
MENULINK_FleetSolutions_FleetInnovation = "Fleet innovation"
MENULINK_FleetSolutions_FleetManagementServices = "Fleet management services"
MENULINK_FleetSolutions_FleetFunding = "Fleet funding"
MENULINK_FleetSolutions_FleetsForGovernment = "Fleets for government"
MENULINK_FleetSolutions_CommercialVehiclesAndTruckLeasing = "Commercial vehicles and truck leasing"
MENULINK_FleetSolutions_SmallFleets = "Small fleets"
MENULINK_FleetSolutions_FleetVehicleAccessories = "Fleet vehicle accessories"
# Next Menu
MENULINK_EmployeeBenefits = "Employee benefits"
MENULINK_EmployeeBenefits_NovatedLease = "Novated lease"
MENULINK_EmployeeBenefits_NewCarShowroom = "New car showroom"
# MENULINK_EmployeeBenefits_OnlineCarTradeinService = "online car trade-in service"
MENULINK_EmployeeBenefits_SalaryPackaging = "Salary packaging"
# Next Menu
MENULINK_DriverSupport = "Driver support"
MENULINK_DriverSupport_FleetDrivers = "Fleet drivers"
MENULINK_DriverSupport_NovatedDrivers = "Novated drivers"
# MENULINK_DriverSupport_NovatedCOVIDSupport = "Novated COVID-19 support"



# ***********************Portfolio - JupiterCloud**************************************
LOGIN_URL = "http://jupiter.cloud.planittesting.com"

# Alert Messages
Expected_Contact_Alert_Main = "We welcome your feedback - but we won't get it unless you complete the form correctly."
Expected_Contact_Alert_Forename = "Forename is required"
Expected_Contact_Alert_Email = "Email is required"
Expected_Contact_Alert_Message = "Message is required"
# Product Price
ExpectedPrice_TeddyBear = "$12.99"
ExpectedPrice_StuffedFrog = "$10.99"
ExpectedPrice_HandmadeDoll = "$10.99"
ExpectedPrice_FluffyBunny = "$9.99"
ExpectedPrice_SmileyBear = "$14.99"
ExpectedPrice_FunnyCow = "$10.99"
ExpectedPrice_ValentineBear = "$14.99"
ExpectedPrice_SmileyFace = "$9.99"
RandomMessage = "This is mandatory - for testing only"
RandomEmail = "Test@planit.net.au"
