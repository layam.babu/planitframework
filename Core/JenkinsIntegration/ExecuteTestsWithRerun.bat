REM   This Batch file is used for Executing Robot Framework Tests in Windows Environment as Shell can't be executed on Windows
REM   Using positional line parameters
REM   %1 = number of processes
REM   %2 = include tag - for Multiple tag options append AND (in capitals, without spaces, don't use & in place of AND) between two tag names. For example if your tag names are like Feature:ABCD TestType:ExtendedRegression, then configure your jobs with Feature:ABCDANDTestType:ExtendedRegression
REM   %3 = environment.py
REM   %4 = output.xml directory relative to the workspace
REM   %5 = location of TestSuites/ folder relative to the workspace
REM   %6 = ENV:UAT,ABC:123,USERNAME:JOHN,PASSWORD:password1 - can be used for jobs with parameters - for multiple values with ',' separated with double quotes
REM   %7 = exclude tag - multiple tags with ',' separated with double quotes ("ABC,def")
REM   %8 = Rerun Flag
REM   Usage :
REM   %WORKSPACE%\Core\JenkinsScripts\ExecuteTestsWithRerun.bat 6 integration-tests Digital\EnvironmentConfigs\UAT-TAM.py Advice\Results Advice\TestSuites ENV:UAT smoke Yes

echo =========================================================================================================================

cd %WORKSPACE%

if exist %WORKSPACE%\%4\output.xml  del /f %WORKSPACE%\%4\output.xml
if exist %WORKSPACE%\%4\rerun.xml  del /f %WORKSPACE%\%4\rerun.xml
if exist %WORKSPACE%\%4\first_run_log.html  del /f %WORKSPACE%\%4\first_run_log.html
if exist %WORKSPACE%\%4\second_run_log.html  del /f %WORKSPACE%\%4\second_run_log.html
echo if exist %WORKSPACE%\%4  RD /s /q "%WORKSPACE%\%4"

set param2=%~2
if NOT DEFINED param2 SET tempInclude=
if DEFINED param2 SET tempInclude=--include %2

set param6=%~6
if NOT DEFINED param6 set var=
if DEFINED param6 (
    set var=
    set param6=%param6: =replace%
    for %%a in (%param6%) do (
       set var=-v "%%a" !var!
       set var=!var:replace= !
    )
)

echo value of param6 is %var%

REM trim spaces from right
for /l %%a in (1,1,31) do if "!var:~-1!"==" " set var=!var:~0,-1!

REM trim spaces from left
for /f "tokens=* delims= " %%a in ("%var%") do set var=%%a

echo value of tempinclude is %tempInclude% and value of var is "%var%"

set param7=%~7
if NOT DEFINED param7 SET tempExclude=
if DEFINED param7 (
    set tempExclude=
    set param7=%param7: =replace%
    for %%i in (%param7%) do (
       set tempExclude=-e %%i !tempExclude!
       set tempExclude=!tempExclude:replace= !
    )
)

echo value of param7 is %tempExclude%

set param8=%~8
if NOT DEFINED param8 SET param8=Yes

echo value of tempExclude is %tempExclude%

SET PATH=C:\Python27\;C:\Python27\Scripts;C:\instantclient_11_2;

cd..
cd C:\Python27\Scripts

IF NOT "%param8%"=="Yes" (
   echo Execting pabot Command
   call pabot --processes %1 --pythonpath %WORKSPACE%:%WORKSPACE%\*:%WORKSPACE%\*\*:%WORKSPACE%\*\*\*:%WORKSPACE%\*\*\*\*:%WORKSPACE%\*\*\*\*\*:%WORKSPACE%\*\*\*\*\*\* --loglevel DEBUG %var% %tempExclude% %tempInclude% --variablefile %WORKSPACE%\%3 --outputdir %WORKSPACE%\%4 --output output.xml %WORKSPACE%\%5
   EXIT 0
)

echo Executing pabot Command
call pabot --processes %1 --pythonpath %WORKSPACE%:%WORKSPACE%\*:%WORKSPACE%\*\*:%WORKSPACE%\*\*\*:%WORKSPACE%\*\*\*\*:%WORKSPACE%\*\*\*\*\*:%WORKSPACE%\*\*\*\*\*\* --loglevel DEBUG %var% %tempExclude% %tempInclude% --variablefile %WORKSPACE%\%3 --outputdir %WORKSPACE%\%4 --output first_run.xml %WORKSPACE%\%5

echo ERROR LEVEL IS %ERRORLEVEL%

echo ======================================
echo FIRST execution is complete
echo ======================================

if %ERRORLEVEL% EQU 0 (
   copy "%WORKSPACE%\%4\first_run.xml" "%WORKSPACE%\%4\output.xml"
   echo "we don't run the tests again as everything was OK on first try"
   EXIT 0
)

echo "we keep a copy of the first log file"
copy "%WORKSPACE%\%4\log.html"  "%WORKSPACE%\%4\first_run_log.html"

echo "#############  Starting the re-execution of failed Tests #################"
call robot --rerunfailed %WORKSPACE%\%4\first_run.xml --pythonpath %WORKSPACE%:%WORKSPACE%\*:%WORKSPACE%\*\*:%WORKSPACE%\*\*\*:%WORKSPACE%\*\*\*\*:%WORKSPACE%\*\*\*\*\*:%WORKSPACE%\*\*\*\*\*\* --loglevel DEBUG %var% %tempExclude% %tempInclude% --listener "%WORKSPACE%\Core\JenkinsScripts\TestListener.py;env=%3,buildurl=%BUILD_URL%" --variablefile %WORKSPACE%\%3 --outputdir %WORKSPACE%\%4 --output rerun.xml %WORKSPACE%\%5


echo  #########################################
echo  RE-EXECUTION is complete
echo  #########################################


echo "we keep a copy of the second log file"
copy "%WORKSPACE%\%4\log.html"  "%WORKSPACE%\%4\second_run_log.html"

echo "merge outputs started"
rebot --nostatusrc --outputdir %WORKSPACE%\%4 --output output.xml --merge %WORKSPACE%\%4\first_run.xml %WORKSPACE%\%4\rerun.xml
echo "merge outputs finished"

EXIT 0