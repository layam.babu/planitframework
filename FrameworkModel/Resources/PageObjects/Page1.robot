*** Settings ***
Resource   Core/CommonResources/Utilities.robot

*** Variables ***
        [Documentation]   List all the variables that are in Page1.robot
        ...       Naming Conventions for the elementName is  ${ELEMENTTYPE_Page_Section_ElementName}
        ...       For Example as below
${INPUT_LoginPage_UserName}                 xpath=//input[.="Username"]
${INPUT_LoginPage_Password}                 xpath=//input[.="Password"]
${Button_LoginPage_Submit}                  xpath=//div[.="Submit"]

*** Keywords ***
page1.actual test step1
    [Documentation]   Actual Code

