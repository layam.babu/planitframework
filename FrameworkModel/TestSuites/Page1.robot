*** Settings ***
Documentation     Model Framework to use across Portfolios
Resource          Core/CommonResources/BaseTest.robot
Resource          FrameworkModel/Resources/StepDefinitions/BDDKeywords.robot
Force Tags        Portfolio:""    ApplicationName:""   TestType:CoreRegression

Test Setup        Base Web Test Setup   ${ENV_BROWSERTYPE}
Test Teardown     Base Web Test Tear Down

*** Variables ***
    [Documentation]   This section is not mandatory as the Variables are defined in the POM file
    ...     any variable that is not on POM but want to send Test Data. This space can be used

*** Test Cases ***
Page1.Test case name
    [Documentation]    The test cases should be developed in Gherkin Language as given below:
    ...      Ensure to add Tag names to run the same scripts in Jenkins
    [Tags]  TestType:SmokeTesting
    Given Keyword1
     And Keyword2
    When Keyword3
     And Keyword4
     And Keyword5
    Then Keyword6
