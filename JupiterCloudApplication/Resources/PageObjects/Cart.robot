*** Settings ***
Resource   Core/CommonResources/Utilities.robot

*** Variables ***
${INT_CartPage_SubTotalFluffyBunny}                 xpath=//img[contains(@ng-src,"bunny")]/ancestor::tr//td[4]
${INT_CartPage_SubTotalTeddyBear}                   xpath=//img[contains(@ng-src,"teddy")]/ancestor::tr//td[4]
${INT_CartPage_SubTotalHandmadeDoll}                xpath=//img[contains(@ng-src,"doll")]/ancestor::tr//td[4]
${INT_CartPage_SubTotalStuffedFrog}                 xpath=//img[contains(@ng-src,"frog")]/ancestor::tr//td[4]
${INT_CartPage_SubTotalSmileyBear}                  xpath=//img[contains(@ng-src,"smiley")]/ancestor::tr//td[4]
${INT_CartPage_SubTotalFunnyCow}                    xpath=//img[contains(@ng-src,"cow")]/ancestor::tr//td[4]
${INT_CartPage_SubTotalSmileyFace}                  xpath=//img[contains(@ng-src,"face")]/ancestor::tr//td[4]
${INT_CartPage_SubTotalValentineBear}               xpath=//img[contains(@ng-src,"valentine")]/ancestor::tr//td[4]
${INT_CartPage_PriceFluffyBunny}                    xpath=//img[contains(@ng-src,"bunny")]/ancestor::tr//td[2]
${INT_CartPage_PriceTeddyBear}                      xpath=//img[contains(@ng-src,"teddy")]/ancestor::tr//td[2]
${INT_CartPage_PriceHandmadeDoll}                   xpath=//img[contains(@ng-src,"doll")]/ancestor::tr//td[2]
${INT_CartPage_PriceStuffedFrog}                    xpath=//img[contains(@ng-src,"frog")]/ancestor::tr//td[2]
${INT_CartPage_PriceSmileyBear}                     xpath=//img[contains(@ng-src,"smiley")]/ancestor::tr//td[2]
${INT_CartPage_PriceFunnyCow}                       xpath=//img[contains(@ng-src,"cow")]/ancestor::tr//td[2]
${INT_CartPage_PriceSmileyFace}                     xpath=//img[contains(@ng-src,"face")]/ancestor::tr//td[2]
${INT_CartPage_PriceValentineBear}                  xpath=//img[contains(@ng-src,"valentine")]/ancestor::tr//td[2]

*** Keywords ***
cartPage.Verify the subtotal for each product is correct
    run keyword if   '${StuffedFrog}'!='0'      cartPage.calculate the Subtotal for each product is correct     StuffedFrog     ${INT_CartPage_SubTotalStuffedFrog}         ${ExpectedPrice_StuffedFrog}        ${StuffedFrog}
    run keyword if   '${FluffyBunny}'!='0'      cartPage.calculate the Subtotal for each product is correct     FluffyBunny     ${INT_CartPage_SubTotalFluffyBunny}         ${ExpectedPrice_FluffyBunny}        ${FluffyBunny}
    run keyword if   '${ValentineBear}'!='0'    cartPage.calculate the Subtotal for each product is correct     ValentineBear   ${INT_CartPage_SubTotalValentineBear}         ${ExpectedPrice_ValentineBear}    ${ValentineBear}
    run keyword if   '${TeddyBear}'!='0'        cartPage.calculate the Subtotal for each product is correct     TeddyBear       ${INT_CartPage_SubTotalTeddyBear}         ${ExpectedPrice_TeddyBear}            ${TeddyBear}
    run keyword if   '${HandmadeDoll}'!='0'     cartPage.calculate the Subtotal for each product is correct     HandmadeDoll    ${INT_CartPage_SubTotalHandmadeDoll}         ${ExpectedPrice_HandmadeDoll}      ${HandmadeDoll}
    run keyword if   '${SmileyBear}'!='0'       cartPage.calculate the Subtotal for each product is correct     SmileyBear      ${INT_CartPage_SubTotalSmileyBear}         ${ExpectedPrice_SmileyBear}          ${SmileyBear}
    run keyword if   '${FunnyCow}'!='0'         cartPage.calculate the Subtotal for each product is correct     FunnyCow        ${INT_CartPage_SubTotalFunnyCow}         ${ExpectedPrice_FunnyCow}              ${FunnyCow}
    run keyword if   '${SmileyFace}'!='0'       cartPage.calculate the Subtotal for each product is correct     SmileyFace      ${INT_CartPage_SubTotalSmileyFace}         ${ExpectedPrice_SmileyFace}          ${SmileyFace}

cartPage.calculate the Subtotal for each product is correct
    [Arguments]     ${itemName}    ${Actual_SubTotal}    ${Expected_IndPrice}    ${itemCount}

    #Actuals
    ${captureActualSubTotal}        get text     ${Actual_SubTotal}
    ${captureActualSubTotal}=    Remove String        ${captureActualSubTotal}   ,    $
    ${captureActualSubTotal}=    Convert To Number    ${captureActualSubTotal}

    #Expected
    ${Expected_IndPrice}=    Remove String        ${Expected_IndPrice}   ,    $
    ${Expected_IndPrice}=    Convert To Number    ${Expected_IndPrice}
    ${captureExpectedSubTotal}=    Evaluate    ${Expected_IndPrice} * ${itemCount}

    run keyword and continue on failure     Should Be Equal As Strings    ${captureExpectedSubTotal}           ${captureActualSubTotal}
    log to console      The expected and Actual SubTotal for ${itemName} is ${captureExpectedSubTotal} and ${captureActualSubTotal} respectively


cartPage.Verify the Price for each product is correct
    run keyword if   '${StuffedFrog}'!='0'      cartPage.verify the Price is correct     StuffedFrog     ${INT_CartPage_PriceStuffedFrog}         ${ExpectedPrice_StuffedFrog}        ${StuffedFrog}
    run keyword if   '${FluffyBunny}'!='0'      cartPage.verify the Price is correct     FluffyBunny     ${INT_CartPage_PriceFluffyBunny}         ${ExpectedPrice_FluffyBunny}        ${FluffyBunny}
    run keyword if   '${ValentineBear}'!='0'    cartPage.verify the Price is correct     ValentineBear   ${INT_CartPage_PriceValentineBear}         ${ExpectedPrice_ValentineBear}    ${ValentineBear}
    run keyword if   '${TeddyBear}'!='0'        cartPage.verify the Price is correct     TeddyBear       ${INT_CartPage_PriceTeddyBear}         ${ExpectedPrice_TeddyBear}            ${TeddyBear}
    run keyword if   '${HandmadeDoll}'!='0'     cartPage.verify the Price is correct     HandmadeDoll    ${INT_CartPage_PriceHandmadeDoll}         ${ExpectedPrice_HandmadeDoll}      ${HandmadeDoll}
    run keyword if   '${SmileyBear}'!='0'       cartPage.verify the Price is correct     SmileyBear      ${INT_CartPage_PriceSmileyBear}         ${ExpectedPrice_SmileyBear}          ${SmileyBear}
    run keyword if   '${FunnyCow}'!='0'         cartPage.verify the Price is correct     FunnyCow        ${INT_CartPage_PriceFunnyCow}         ${ExpectedPrice_FunnyCow}              ${FunnyCow}
    run keyword if   '${SmileyFace}'!='0'       cartPage.verify the Price is correct     SmileyFace      ${INT_CartPage_PriceSmileyFace}         ${ExpectedPrice_SmileyFace}          ${SmileyFace}

cartPage.verify the Price is correct
    [Arguments]     ${itemName}    ${Actual_Price}    ${Expected_IndPrice}    ${itemCount}

    #Actuals
    ${captureActualPrice}        get text     ${Actual_Price}
    ${captureActualPrice}=    Remove String        ${captureActualPrice}   ,    $
    ${captureActualPrice}=    Convert To Number    ${captureActualPrice}

    #Expected
    ${captureExpectedPrice}=    Remove String        ${Expected_IndPrice}   ,    $
    ${captureExpectedPrice}=    Convert To Number    ${captureExpectedPrice}


    run keyword and continue on failure    Should Be Equal As Strings    ${captureExpectedPrice}           ${captureActualPrice}
    log to console      The expected and Actual Price for ${itemName} is ${captureExpectedPrice} and ${captureActualPrice} respectively
