*** Settings ***
Resource   Core/CommonResources/Utilities.robot


*** Variables ***
${BUTTON_ContactPage_Submit}          xpath=//a[.="Submit"]
${ALERT_ContactPage_Main}             xpath=//div[contains(@class,"alert")]
${ALERT_ContactPage_Forename}         xpath=//span[.="Forename is required"]
${ALERT_ContactPage_Email}            xpath=//span[.="Email is required"]
${ALERT_ContactPage_Message}          xpath=//span[.="Message is required"]
${INPUT_ContactPage_Forename}         id=forename
${INPUT_ContactPage_Email}            id=email
${TEXTAREA_ContactPage_Message}       id=message


*** Keywords ***
contactPage.click on the Header-Contact
    wait until element is visible    ${MENU_HomePage_Contact}
    click element      ${MENU_HomePage_Contact}

contactPage.click Submit button
    wait until element is visible    ${BUTTON_ContactPage_Submit}
    click element      ${BUTTON_ContactPage_Submit}

contactPage.Validate errors are gone
    ${notDisplayedForenameAlert}=  run keyword and ignore error    element should not be visible     ${ALERT_ContactPage_Forename}
    run keyword if   "${notDisplayedForenameAlert}"=="False"   fail   Forename Alert is still displayed, not as expected

    ${notDisplayedEmailAlert}=  run keyword and ignore error   element should not be visible     ${ALERT_ContactPage_Email}
    run keyword if   "${notDisplayedEmailAlert}"=="False"   fail   Email Alert is still displayed, not as expected

    ${notDisplayedMessageAlert}=  run keyword and ignore error   element should not be visible     ${ALERT_ContactPage_Message}
    run keyword if   "${notDisplayedMessageAlert}"=="False"   fail   Email Alert is still displayed, not as expected


contactPage.Verify error messages
    ${Actual_Contact_Alert_Main}    get text   ${ALERT_ContactPage_Main}
    Should Be Equal As Strings    ${Actual_Contact_Alert_Main}    ${Expected_Contact_Alert_Main}

    ${Actual_Contact_Alert_Forename}    get text   ${ALERT_ContactPage_Forename}
    Should Be Equal As Strings    ${Actual_Contact_Alert_Forename}    ${Expected_Contact_Alert_Forename}

    ${Actual_Contact_Alert_Email}    get text   ${ALERT_ContactPage_Email}
    Should Be Equal As Strings    ${Actual_Contact_Alert_Email}    ${Expected_Contact_Alert_Email}

    ${Actual_Contact_Alert_Message}    get text   ${ALERT_ContactPage_Message}
    Should Be Equal As Strings    ${Actual_Contact_Alert_Message}    ${Expected_Contact_Alert_Message}

    #other approach to compare
    #    ${alert_Forename_Present}= run keyword and return status    wait until element is visible    ${ALERT_ContactPage_Forename}
    #    run keyword if    ${alert_Forename_Present} is True     log to console  "The element is present, as expected"


contactPage.Populate mandatory fields
    ${randomForeName}=  Utilities.Generate random applicant name  5
    input text    ${INPUT_ContactPage_Forename}    ${randomForeName}

    input text    ${INPUT_ContactPage_Email}    ${RandomEmail}

    input text    ${TEXTAREA_ContactPage_Message}    ${RandomMessage}

contactPage.Validate successful submission message
    set selenium speed   1s
        FOR  ${index}  IN RANGE   1  10
            ${popupSendingFeedback}=  run keyword and return status    element should not be visible     //h1[.="Sending Feedback"]
            exit for loop if    '${popupSendingFeedback}' == 'True'
        END
    set selenium speed   0s

    BuiltIn.Wait Until Keyword Succeeds   2 times  1s    Wait until element is visible    ${ALERT_ContactPage_Main}

    ${Actual_Contact_SubmitAlert}    get text   ${ALERT_ContactPage_Main}
    log    ${Actual_Contact_SubmitAlert}