*** Settings ***
Resource   Core/CommonResources/Utilities.robot


*** Variables ***
${MENU_HomePage_Contact}              xpath=//a[contains(.,"Contact")]
${MENU_HomePage_Shop}                 xpath=//a[contains(.,"Shop")]
${MENU_HomePage_Cart}                 xpath=//span[contains(@class,"cart")]/parent::a

*** Keywords ***
homePage.click on the Header-Contact
    wait until element is visible    ${MENU_HomePage_Contact}
    click element      ${MENU_HomePage_Contact}

homePage.click on the Header-Shop
    wait until element is visible    ${MENU_HomePage_Shop}
    click element      ${MENU_HomePage_Shop}

homePage.click on the Header-Cart
    wait until element is visible    ${MENU_HomePage_Cart}
    click element      ${MENU_HomePage_Cart}


