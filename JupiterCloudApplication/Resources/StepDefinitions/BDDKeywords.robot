*** Settings ***
Resource   JupiterCloudApplication/Resources/PageObjects/Contact.robot
Resource   JupiterCloudApplication/Resources/PageObjects/Home.robot
Resource   JupiterCloudApplication/Resources/PageObjects/Shop.robot
Resource   JupiterCloudApplication/Resources/PageObjects/Cart.robot
Resource   Core/CommonResources/Utilities.robot

*** Variables ***

*** Keywords ***
I log into JupiterCloud applicaion
    Go to   ${LOGIN_URL}
    Maximize Browser Window

From the home page go to contact page
    homePage.click on the Header-Contact

From the home page go to Shop page
    homePage.click on the Header-Shop

Click submit button
    contactPage.click Submit button

Verify error messages
    contactPage.Verify error messages

Populate mandatory fields
    contactPage.Populate mandatory fields

Validate errors are gone
    contactPage.Validate errors are gone

Validate successful submission message
    contactPage.Validate successful submission message

Buy products and validate the expected price in Shopping page
    [Arguments]   ${StuffedFrog}=    ${FluffyBunny}=     ${ValentineBear}=    ${TeddyBear}=     ${HandmadeDoll}=     ${SmileyBear}=    ${FunnyCow}=     ${SmileyFace}=

    Set Test Variable    ${StuffedFrog}     ${StuffedFrog}
    Set Test Variable    ${FluffyBunny}     ${FluffyBunny}
    Set Test Variable    ${ValentineBear}   ${ValentineBear}
    Set Test Variable    ${TeddyBear}       ${TeddyBear}
    Set Test Variable    ${HandmadeDoll}    ${HandmadeDoll}
    Set Test Variable    ${SmileyBear}      ${SmileyBear}
    Set Test Variable    ${FunnyCow}        ${FunnyCow}
    Set Test Variable    ${SmileyFace}      ${SmileyFace}

    run keyword if   '${StuffedFrog}'!='0'          shopPage.Capture the Price and Add to cart   ${StuffedFrog}      ${TEXT_ShopPage_StuffedFrog_Price}      ${LINK_ShopPage_StuffedFrog_Buy}     StuffedFrog
    run keyword if   '${FluffyBunny}'!='0'          shopPage.Capture the Price and Add to cart   ${FluffyBunny}      ${TEXT_ShopPage_FluffyBunny_Price}      ${LINK_ShopPage_FluffyBunny_Buy}     FluffyBunny
    run keyword if   '${ValentineBear}'!='0'        shopPage.Capture the Price and Add to cart   ${ValentineBear}    ${TEXT_ShopPage_ValentineBear_Price}    ${LINK_ShopPage_ValentineBear_Buy}   ValentineBear
    run keyword if   '${TeddyBear}'!='0'            shopPage.Capture the Price and Add to cart   ${TeddyBear}        ${TEXT_ShopPage_TeddyBear_Price}        ${LINK_ShopPage_TeddyBear_Buy}       TeddyBear
    run keyword if   '${HandmadeDoll}'!='0'         shopPage.Capture the Price and Add to cart   ${HandmadeDoll}     ${TEXT_ShopPage_HandmadeDoll_Price}     ${LINK_ShopPage_HandmadeDoll_Buy}    HandmadeDoll
    run keyword if   '${SmileyBear}'!='0'           shopPage.Capture the Price and Add to cart   ${SmileyBear}       ${TEXT_ShopPage_SmileyBear_Price}       ${LINK_ShopPage_SmileyBear_Buy}      SmileyBear
    run keyword if   '${FunnyCow}'!='0'             shopPage.Capture the Price and Add to cart   ${FunnyCow}         ${TEXT_ShopPage_FunnyCow_Price}         ${LINK_ShopPage_FunnyCow_Buy}        FunnyCow
    run keyword if   '${SmileyFace}'!='0'           shopPage.Capture the Price and Add to cart   ${SmileyFace}       ${TEXT_ShopPage_SmileyFace_Price}       ${LINK_ShopPage_SmileyFace_Buy}       SmileyFace


Verify the subtotal for each product is correct in Cart Page
    homePage.click on the Header-Cart
    cartPage.Verify the subtotal for each product is correct

Verify the price for each product is correct in Cart Page
    cartPage.Verify the Price for each product is correct
