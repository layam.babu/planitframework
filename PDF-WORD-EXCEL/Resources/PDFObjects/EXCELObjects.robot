*** Settings ***
Resource   Core/CommonResources/Utilities.robot

*** Variables ***


*** Keywords ***
Excel.capture the COLUMN number for CustomerNumber
    [Arguments]       ${searchString}
    #Retrive Data

    FOR     ${colIndex}    IN RANGE   0   ${Col_ID}
        ${data}     Read Cell Data By Coordinates   ${Excel_Sheetname}   ${colIndex}     0
#        run keyword if    '${data}'=='${searchString}'   set test variable    ${Actual_Col}     ${data}
        IF    '${data}'=='${searchString}'      BREAK
    END
    log to console    ${colIndex}
    [Return]    ${colIndex}


Excel.capture the ROW number and Return the data available for PDF CustomerNumber
    [Arguments]    ${colNo}      ${searchString}

    FOR    ${rowIndex}     IN RANGE    1    ${Row_ID}
    #calling another loop named Inner loop to read the column of a row passing row number as parameter
        ${data}     Read Cell Data By Coordinates   ${Excel_Sheetname}  ${colNo}     ${rowIndex}
#        run keyword if    '${data}'=='${searchString}'   set test variable    ${Actual_Col}     ${data}
        IF    '${data}'=='${searchString}'      BREAK

        log to console      ${rowIndex}
    END
    [Return]    ${rowIndex}



