*** Settings ***
Resource            PDF-WORD-EXCEL/Resources/PDFObjects/PDFObjects.robot
Resource            PDF-WORD-EXCEL/Resources/PDFObjects/EXCELObjects.robot
Resource            Core/CommonResources/Utilities.robot
Library             Collections
Library             String
Library             Pdf2TextLibrary

*** Variables ***


*** Keywords ***
I convert the PDF file into text document
    [Arguments]       ${FilePath_PDF}
    Utilities.convert PDF to text    ${FilePath_PDF}

capture the Unique key - Customer number from the PDF data
    [Arguments]         ${PDF_Data_detail}
    ${data_CustomerNumber}  PDF.capture 'Customer number' data     ${PDF_Data_detail}
    Set Test Variable     ${PDFdata_CustomerNumber}      ${data_CustomerNumber}

I open the Excel and read Row and Column count
    [Arguments]        ${FilePath_EXCEL}     ${Excel_Sheetname}
        Utilities.Open excel file and read row and column count     ${FilePath_EXCEL}       ${Excel_Sheetname}

capture the actual excel row data
    [Arguments]    ${CONSTANT_Customer_Number}      ${PDFdata_CustomerNumber}
    ${Excel_Col_No} =    Excel.capture the COLUMN number for CustomerNumber     ${CONSTANT_Customer_Number}
    Set Test Variable    ${Excel_Col_No}       ${Excel_Col_No}
    ${Excel_Row_No} =    Excel.capture the ROW number and Return the data available for PDF CustomerNumber     ${Excel_Col_No}      ${PDFdata_CustomerNumber}
    Set Test Variable    ${Excel_Row_No}       ${Excel_Row_No}

validate the Excel and PDF data
    [Arguments]      ${PDFdata_CustomerNumber}    ${Col_ID}     ${Row_ID}

       ${Exceldata_CustomerNumber}     Read Cell Data By Coordinates   ${Excel_Sheetname}      ${Col_ID}     ${Row_ID}
         Should be equal        ${Exceldata_CustomerNumber}     ${PDFdata_CustomerNumber}


