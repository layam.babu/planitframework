*** Settings ***
Resource   Core/CommonResources/Utilities.robot

*** Variables ***
${LINK_HomePage_SGFleet}                //img[@title="Home"]
${LINK_HomePage_Menu_AboutUs}                    //a[.="About us"]
${LINK_HomePage_Menu_FleetSolutions}             //a[.="Fleet solutions"]
${LINK_HomePage_Menu_EmployeeBenefits}           //a[.="Employee benefits"]
${LINK_HomePage_Menu_DriverSupport}             //a[.="Driver support"]

*** Keywords ***
HomePage.validate SGFleet image is displayed on the screen
    element should be visible    ${LINK_HomePage_SGFleet}

HomePage.Validate if the Drop down "Menu Items and Target Links" are as expected
    [Arguments]    ${LINK_MAINMENU}      @{MenuElements}

    FOR  ${item}  IN  @{MenuElements}
        exit for loop if   '${item}'=='0'
        log to console    Clicking the MenuItem: ${item}
        click element    ${LINK_MAINMENU}
        Set selenium speed  0.6s
        click element    //ul[@class="dropdown-menu show"]//a[.="${item}"]
        Utilities.log the script activities into Text File     Clicking the SubMenuItem: ${item}
        ${Actual_MenuItem}   convert to lower case   ${item}
        Utilities.log the script activities into Text File     Converted to lower case: ${Actual_MenuItem}
        ${Actual_MenuItem}   replace string   ${Actual_MenuItem}    ${SPACE}    -
        Utilities.log the script activities into Text File     Replace SpaceWithHypen: ${Actual_MenuItem}
        ${Actual_WindowTitle}=   get Location
        Utilities.log the script activities into Text File     Captured window Title : ${Actual_WindowTitle}
        run keyword and continue on failure      Should contain      ${Actual_WindowTitle}    ${Actual_MenuItem}
        log to console  The actual and expected Window Title is: ${Actual_WindowTitle}
        Utilities.log the script activities into Text File     Actual String ${Actual_MenuItem} is displayed as Expected for the Target Link URL - ${Actual_WindowTitle}
        click element    ${LINK_HomePage_SGFleet}
    END
    Set selenium speed  0s

