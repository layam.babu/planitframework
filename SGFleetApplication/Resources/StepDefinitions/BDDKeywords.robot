*** Settings ***
Resource   SGFleetApplication/Resources/PageObjects/HomePage.robot
Resource   Core/CommonResources/Utilities.robot

*** Variables ***


*** Keywords ***
I login to SG Fleet application
    go to            ${SG_LOGIN_URL}
    Utilities.log the script activities into Text File     "Login to SGFleet Application is Successful"

Home page is successfully launched
    HomePage.validate SGFleet image is displayed on the screen
    Utilities.log the script activities into Text File     "Home Page is displayed successfully"

Validate the Drop down "Menu Items and Target Links"
    [Arguments]   ${MENULINK1}=0    ${MENULINK2}=0    ${MENULINK3}=0    ${MENULINK4}=0    ${MENULINK5}=0    ${MENULINK6}=0    ${MENULINK7}=0    ${MENULINK8}=0    ${MENULINK9}=0
    Set test Variable     ${LINK_MAINMENU}     ${MENULINK1}
    @{MenuElements}=    capture the "Menu Items" Test Data from the config files   ${MENULINK2}   ${MENULINK3}   ${MENULINK4}   ${MENULINK5}   ${MENULINK6}  ${MENULINK7}    ${MENULINK8}    ${MENULINK9}
    HomePage.Validate if the Drop down "Menu Items and Target Links" are as expected    ${LINK_MAINMENU}    @{MenuElements}

capture the "Menu Items" Test Data from the config files
    [Arguments]   ${MENULINK2}=0    ${MENULINK3}=0    ${MENULINK4}=0    ${MENULINK5}=0    ${MENULINK6}=0    ${MENULINK7}=0    ${MENULINK8}=0    ${MENULINK9}=0

    @{MenuElements} =  create list   ${MENULINK2}   ${MENULINK3}   ${MENULINK4}   ${MENULINK5}   ${MENULINK6}  ${MENULINK7}    ${MENULINK8}    ${MENULINK9}
    Utilities.log the script activities into Text File     "Reading the Test Data from Config - PASSED for " @{MenuElements}

    [Return]    @{MenuElements}