*** Settings ***
Documentation     Model Framework to use across Portfolios
Resource          Core/CommonResources/BaseTest.robot
Resource          SGFleetApplication/Resources/StepDefinitions/BDDKeywords.robot
Force Tags        Portfolio:CommercialFinance    ApplicationName:SGFleet   TestType:HealthCheck

Test Setup        Base Web Test Setup   ${ENV_BROWSERTYPE}
Test Teardown     Base Web Test Tear Down


*** Variables ***

*** Test Cases ***
HomePage.Validate the Navigation Bar functionality
    Given I login to SG Fleet application
    When Home page is successfully launched
    Then Validate the Drop down "Menu Items and Target Links"    ${LINK_HomePage_Menu_AboutUs}   ${MENULINK_AboutUs}  ${MENULINK_AboutUs_OurStory}  ${MENULINK_AboutUs_GlobalFleetManagement}  ${MENULINK_AboutUs_WhyChooseUs}  ${MENULINK_AboutUs_CorporateSocialResponsibility}  ${MENULINK_AboutUs_News}
     And Validate the Drop down "Menu Items and Target Links"    ${LINK_HomePage_Menu_FleetSolutions}   ${MENULINK_FleetSolutions}  ${MENULINK_FleetSolutions_FleetInnovation}  ${MENULINK_FleetSolutions_FleetManagementServices}   ${MENULINK_FleetSolutions_FleetFunding}  ${MENULINK_FleetSolutions_FleetsForGovernment}  ${MENULINK_FleetSolutions_CommercialVehiclesAndTruckLeasing}  ${MENULINK_FleetSolutions_SmallFleets}  ${MENULINK_FleetSolutions_FleetVehicleAccessories}
     And Validate the Drop down "Menu Items and Target Links"    ${LINK_HomePage_Menu_EmployeeBenefits}  ${MENULINK_EmployeeBenefits}    ${MENULINK_EmployeeBenefits_NovatedLease}   ${MENULINK_EmployeeBenefits_NewCarShowroom}     ${MENULINK_EmployeeBenefits_SalaryPackaging}
     And Validate the Drop down "Menu Items and Target Links"    ${LINK_HomePage_Menu_DriverSupport}    ${MENULINK_DriverSupport}    ${MENULINK_DriverSupport_FleetDrivers}    ${MENULINK_DriverSupport_NovatedDrivers}   0










# Stale element error
#    @{elem} =   Get WebElements     //ul[@class="dropdown-menu show"]//a
#    FOR  ${item}  IN  @{elem}
#        log to console  Item: ${item.text}
#        ${Actual_MenuItem}   set variable    ${item.text}
#        ${Actual_MenuItem}   convert to lower case   ${Actual_MenuItem}
#        ${Actual_MenuItem}   replace string   ${Actual_MenuItem}    ${SPACE}    -
#         Wait Until Keyword Succeeds    4 times    0.1 s     click element      ${item}
#        ${Actual_WindowTitle}=   get Location
#        Should contain      ${Actual_WindowTitle}    ${Actual_MenuItem}
#        log to console  Window Title: ${Actual_WindowTitle}
#        click element   //img[@title="Home"]
#        click element    id=navbarDropdownMenuLink-1
#    END